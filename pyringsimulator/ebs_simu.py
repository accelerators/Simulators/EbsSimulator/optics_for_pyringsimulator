import tango
import time
import ComputeOptics as co
from threading import Thread, ThreadError, Timer, Lock


class ComputeOpticsEventsAction:

    def __init__(self, _co, _dev):
        self.co = _co  # ComputeOptics instance
        self.event_subscribe_call = False
        self.dev = _dev

    def push_event(self, eve):
        # use even type to switch among different actions
        if eve.event == 'change':
            print('what you want')
            try:
                if not self.event_subscribe_call:
                    self.event_subscribe_call = True
                else:
                    print('Save file to: {}'.format(eve.attr_value.value))
                    self.co.save_ring(eve.attr_value.value)

            except AttributeError:
                print('Save file failed or no Save required')

        elif eve.event == 'data_ready':
            # each commented line below has been tested without success
            print('reset ebs simu')
            print('FROZEN magnets ARE IGNORED AND SET TO MODEL VALUES')
            print('reset ebs simu')


def ComputeOpticsLoop(_name, _file, _des_ring, _err_ring, _cor_ring, _verbose):
    tango.ApiUtil.cleanup()

    print('file = ', _file)
    print('des ring = ', _des_ring)
    print('err ring = ', _err_ring)
    print('cor ring = ', _cor_ring)
    print('verbose = ', _verbose)

    ctr = 0
    dev = tango.DeviceProxy(_name)
    p = dev.get_property('LoggingFilePath')
    _logfile = p['LoggingFilePath'][0] + '/EBS_simulator.log'

    # initialize optics model, dictionary and data using dev properties

    C = co.ComputeOptics(_file,      # matlab file containing at least 3 variables with names defined by following arguments
                         _des_ring,  # lattice without errors without corrections
                         _err_ring,  # lattice with errors and no corrections
                         _cor_ring,  # lattice with errors and with corrections. used for simulations
                         _logfile,
                         _verbose)

    Cev = ComputeOpticsEventsAction(C, dev)

    dev.RingName = _file

    dev.RfFrequency = C.rf_frequency
    dev.RfVoltage = C.rf_voltage  # this also moves from MOVING to ON
    dev.TbT_InCoord = [0.0]*6
    dev.TbT_BufferSize = int(5)

    dev.subscribe_event("SavedFile", tango.EventType.CHANGE_EVENT, Cev) # save a lattice
    dev.subscribe_event("Mode", tango.EventType.DATA_READY_EVENT, Cev) # run ResetSimulator

    #    mag_length = np.array([.311896,0.227687,0.178619,0.227687,0.227687,0.398656,0.492637,0.17989,0.21389,0.17989,0.097688,0.1,0.1,0.1,0.403,0.227687,0.311896,1.0447,0.82515])
    #    dev.MagnetLength = mag_length


    try:
        while True:

            if dev.state() == tango.DevState.MOVING:

                C.reset_simulator()
                #C.rf_voltage = dev.RfVoltage
                dev.RfVoltage = C.rf_voltage  # if RUNNING will switch to ON (always_executed_hook in PyRingSimulator)
                dev.RfFrequency = C.rf_frequency  # must be after or will give error due to state machine refusing setting of attributes while MOVING

            else:

                # get info from ring simulator DS
                C.mode = dev.Mode
                try:
                    C.rad_on = dev.Radiation
                except Exception:
                    print('no Radiation Attribute')

                C.atx_every = dev.AtxEvery
                C.rf_frequency = dev.RfFrequency
                C.rf_voltage = dev.RfVoltage
                dev.RfVoltage = C.rf_voltage  # if MOVING will switch to ON

                C.bpm_noise = dev.BpmNoise
                C.quad_grad_jit = dev.QuadGradJit
                C.quad_vib_ampl = dev.QuadVibAmpl
                C.tbt__in_coord = dev.TbT_InCoord
                C.tbt__buffer_size = dev.TbT_BufferSize

                # get new strengths, if any
                strengths, devnames = dev.GetUpdStrengths()
                for s, d in zip(strengths, devnames):
                    print("changed: {} to {}".format(d, s))

                # time.sleep(5)
                C.get_modified_strengths(strengths, devnames)

                # temporary
                # to run without simulator device
                # C.check_ring_modified()
                #time.sleep(2)

                # update lattice with new strengths
                C.update_ring()

                # compute optics using the updated lattice
                data, tbt_data, optics_ok = C.update_data()

                # set status message
                if optics_ok:
                    dev.ErrMessage = ""  # return to ON
                else:
                    dev.ErrMessage = "Optics computation FAILED"  # switch to ALARM

                # in case no one pressed ResetSimu while computing optics parameters
                if dev.state() != tango.DevState.MOVING:

                    # send data to device
                    if C.mode == co.Mode.TbT:
                        dev.PushTbTRawData(tbt_data)
                        dev.PushRawData(data)
                    else:
                        dev.PushRawData(data)

                    print('ctr = {}, device state = {}, device status = {}'.format(ctr, dev.state(), dev.status()))
                    ctr = ctr + 1


    except tango.DevFailed as e:
        mess = "Tango Error during simulation loop. Loop interrupted, please restart Simulator DS: on Astor: Level 1, RingSimulator/ebs"
        mess = mess + "\nError: " + e.args[0].desc + ". --- beam lost --- please restart DS and call command ResetSimu."
        dev.ErrMessage = mess
    #except Exception as e:
    #    mess = "Python Error"
    #    mess = mess + "\nError: " + e.args[0]
    #    dev.ErrMessage = mess


