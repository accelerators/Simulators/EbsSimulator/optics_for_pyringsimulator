# imports
from ringcontrol.utils.tango import Attribute
import at
from at.lattice.utils import checkname
from at.lattice import get_refpts
import numpy as np
import time
from datetime import datetime
from threading import Thread, ThreadError, Timer, Lock
import os
import bpm_modifications
import enum
from matplotlib import pyplot as plt
from functools import partial
import logging
import logging.handlers as handlers
import sys

__doc__ = """
updates lattice optics according to strengths in simulator tango hosts

to test 

import ComputeOptics as co
co.test_script()


"""
__author__ = 'S. M. Liuzzo'
__email__ = 'simone.liuzzo@esrf.fr'

__all__ = ["ComputeOptics", "main"]

__logfile__ = '/tmp/py_EBS_simulator/EBS_simulator.log'


class OpticsComputationError(Exception):
    """ raised when computation of storage ring lattice optics  has failed """
    pass


class Mode(enum.IntEnum):
    """Python enumerated type for Mode attribute."""
    TbT = 0
    Atlinopt = 1
    Atx = 2
    Mixed = 3
    DetailedAtx = 4


class ComputeOptics(object):
    """
    run

    loads an AT lattice and updates strengths and RF frequency according to those given (or by checking them)
    the AT lattice must have the field Device stating the name of the device in the tango control system
    computes optics parameters : tunes, chromaticy, emittances (if required), orbit, dispersion, beta functions
    computes Turn by Turn data 
    
    input arguments for class initialization are the default lattice optics and variable names, such as:
    lat_file = /operation/beamdyn/matlab/optics/sr/theory/betamodel.mat
    lat_var = betamodel # reference lattice
    lat_var_err = betamodel # lattice with fitted errors
    lat_var_cor = betamodel # lattice with fitted errors and corrections  <- this is the lattice that will be loaded by ResetSimulator
    
    or 
    lat_file = /machfs/MDT/2021/2021_03_01/resp1/FittedLatticeFit.mat
    lat_var = r0     # reference lattice
    lat_var_err = r_fit  # lattice with fitted errors
    lat_var_cor = r_cor  # lattice with fitted errors and corrections. 
    
    The model optics loaded here should correspond to the ones used for operation.
    Otherwise the DesignStrengths fields in the single magnet devices will not be correct.
    
    """

    def __init__(self,
                 lat_path="/machfs/liuzzo/EBS/beamdyn/matlab/optics/sr/theory/betamodel.mat",
                 lat_var="betamodel",
                 lat_var_err="betamodel",
                 lat_var_cor="betamodel",
                 logfile=__logfile__,
                 verb=False
                 ):

        # define logging of informations
        if verb:
            lev = logging.DEBUG
        else:
            lev = logging.INFO

        print('logging info in: {}'.format(logfile))
        self.logfile = logfile
        try:
            os.remove(logfile)  # start a new log file
        except:
            print('not possible to remove file')

        if os.path.isfile(logfile) is not True:
            print('LOG file does not exist. CREATE IT.')
            os.makedirs(os.path.dirname(logfile), exist_ok=True)

            os.umask(0)
            with open(os.open(logfile, os.O_CREAT | os.O_WRONLY, 0o777), 'w') as fh:
                fh.write('EBS simulator log file')

        # logging.basicConfig(filename=logfile,
        #                     filemode='w',
        #                     level=lev,
        #                     format='%(levelname)s %(asctime)s %(message)s',
        #                     datefmt='%m/%d/%Y %I:%M:%S %p')

        log_formatter = logging.Formatter('%(levelname)s %(asctime)s %(message)s',
                                          datefmt='%m/%d/%Y %I:%M:%S %p'
                                          )
        
        log_handler = logging.handlers.RotatingFileHandler(logfile,
                                                           mode='a', maxBytes=5*1024*1024, backupCount=1,
                                                           encoding=None, delay=0)
        cons_handler = logging.StreamHandler(sys.stdout)
        cons_handler.setLevel(lev)

        log_handler.setFormatter(log_formatter)
        log_handler.setLevel(lev)

        self.log = logging.getLogger('root')
        self.log.setLevel(lev)
        self.log.addHandler(log_handler)

        # log_handler = handlers.RotatingFileHandler(logfile, maxBytes=100, backupCount=0)
        # log_handler.setLevel(lev)
        # logger = logging.getLogger()
        # logger.addHandler(log_handler)


        # tango host
        present_host = os.environ['TANGO_HOST']
        if (present_host.startswith('ebs-simu') is not True) and (present_host.startswith('ebssimul.esrf.fr') is not True):
            self.TangoHost = 'ebs-simu:10000'
            os.environ['TANGO_HOST'] = self.TangoHost
            self.log.info("FORCE tango host to simulator for test: " + os.environ['TANGO_HOST'])

        self.TangoHost = os.environ['TANGO_HOST']
        self.log.info("tango host is: " + self.TangoHost )

        self.verbose = verb  # print or not text messages

        self.state = 'ON'  # for future, if needed

        # lattice optics quantities computed by this Class
        self.tune_h = 0.0
        self.tune_v = 0.0
        self.chromaticity_h = 0.0
        self.chromaticity_v = 0.0
        self.loop_time = 0.0
        self.emittance_h = 0.0
        self.emittance_v = 0.0
        self.closed_orbit_h = (0.0,)
        self.closed_orbit_v = (0.0,)
        self.beta_h = (0.0,)
        self.beta_v = (0.0,)
        self.eta_h = (0.0,)
        self.eta_v = (0.0,)
        self.h_positions_tb_t = ((0.0,),)
        self.v_positions_tb_t = ((0.0,),)

        self.closed_orbit_h_pin = [0.0, 0.0, 0.0, 0.0, 0.0]
        self.closed_orbit_v_pin = [0.0, 0.0, 0.0, 0.0, 0.0]

        self.sigmah_pin = [0.0, 0.0, 0.0, 0.0, 0.0]
        self.sigmav_pin = [0.0, 0.0, 0.0, 0.0, 0.0]
        self.sigmahv_pin = [0.0, 0.0, 0.0, 0.0, 0.0]

        # variables required for computation of optics
        self.bpm_noise = 0.0
        self.quad_grad_jit = 0.0
        self.quad_vib_ampl = 0.0
        self.tbt__buffer_size = 0.0
        self.tbt__in_coord = (0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

        self.mode = Mode.Atx
        self.log.debug('Mode is: {}'.format(self.mode))

        # other variables and flags
        self.ring_name = ''
        self.err_message = ''
        self.simulation_started = False
        self.mat_save_ring = False  # flag to say if lattice has been saved
        self.mat_mode = 0.0

        self.magnet_length = (0.0,)
        self.bpm_position = (0.0,)
        self.id_markers = (0.0,)
        self.detailed_atx_data = ((0.0,),)

        # counters
        self.atx_every = 1.0
        self.counter = 0.0
        self.detailed_atx_counter = 0.0

        # initialize BPM errors.
        self.rel = []
        self.tel = []
        self.trand = []

        # define AT lattice
        self.path_to_lattice_model = lat_path
        self.variable_in_lattice_file = lat_var
        self.variable_in_lattice_file_errors = lat_var_err
        self.variable_in_lattice_file_corrections = lat_var_cor

        # initialization actions that must be called also by self.reset_simulator
        self.initialize(
            lat_path,
            lat_var,
            lat_var_err,
            lat_var_cor,
            verb
            )

        # # initialize Locks and Threads
        self.lockrunning = Lock()  # lock to check if som other optimization is already running

        pass

    def initialize(self,
                 lat_path="/machfs/liuzzo/EBS/beamdyn/matlab/optics/sr/theory/betamodel.mat",
                 lat_var="betamodel",
                 lat_var_err="betamodel",
                 lat_var_cor="betamodel",
                 verb=False
                 ):
        """Initialises ComputeOptics.

        input arguments
        lat_path = "/machfs/liuzzo/EBS/beamdyn/matlab/optics/sr/theory/betamodel.mat",
        lat_var = "betamodel",
        lat_var_err = "betamodel",
        lat_var_cor = "betamodel"

        """

        self.rad_on = False

        # reference lattice with errors and correction
        try:
            self.ring = at.load_lattice(self.path_to_lattice_model, key=lat_var_cor)
            self.ring = at.Lattice(self.ring)
            if self.rad_on:
                self.ring.radiation_on() # radiation on once and for all
        except FileNotFoundError as err:
            logging.warning('variable {} or lattice file (MODEL+ERR+COR) {} are probably incorrect.'.format(
                lat_var_cor, self.path_to_lattice_model))
            raise err

        # lattice with errors only, no corrections
        try:
            self.ring_err = at.load_lattice(self.path_to_lattice_model, key=lat_var_err)
            self.ring_err = at.Lattice(self.ring_err)
        except FileNotFoundError as err:
            logging.warning('variable {} or lattice file (MODEL+ERR) {} probably incorrect.'.format(
                lat_var_err, self.path_to_lattice_model))
            raise err

        # lattice without errors and without corrections
        try:
            self.ring_0 = at.load_lattice(self.path_to_lattice_model, key=lat_var)
            self.ring_0 = at.Lattice(self.ring_0)
        except FileNotFoundError as err:
            logging.warning('variable {} or lattice file (MODEL) {} probably incorrect.'.format(
                lat_var, self.path_to_lattice_model))
            raise err

        # get indexes (once and for all)
        # elems = self.ring.get_elements('BPM*') or self.ring.get_elements(at.element.Monitor)
        # self.ind_bpms = [self.ring.index(elem) for elem in elems]

        self.ind_bpms = list(get_refpts(self.ring, 'BPM*'))

        self.bpm_position = self.ring.get_s_pos(refpts=self.ind_bpms)

        self.ind_rf = list(get_refpts(self.ring, 'CAV*'))
        self.ind_pinhole = list(get_refpts(self.ring, 'PINHOLE*'))

        # print(self.ind_pinhole)
        # print(self.ind_bpms)
        # print(self.ind_rf)

        if len(self.ind_pinhole) == 0:
            pinhole_id = (7 - 1 - 4, 25 - 1 - 4)   # -1 for python, -4 for cell numbering starting at 4
            pinhole_bm = (9 - 1 - 4, 17 - 1 - 4, 27 - 1 - 4)
            logging.warning('using DL1A and mlDQ1D indexes to mark pinholes, in official lattice the source '
                            'is placed slicing the magnets')

            # rough location of pinholes, as entrance of DL1A, mlDQ1D, no splitting done here.
            elems_DL  = self.ring.get_elements('DL1A*')
            elems_DQ = self.ring.get_elements('mlDQ1D*')

            elems_DLsel = [elems_DL[ii] for ii in pinhole_id]
            elems_DQsel = [elems_DQ[ii] for ii in pinhole_bm]

            self.ind_pinhole_dl = [self.ring.index(elem) for elem in elems_DLsel]
            self.ind_pinhole_dq = [self.ring.index(elem) for elem in elems_DQsel]

            self.ind_pinhole = self.ind_pinhole_dl + self.ind_pinhole_dq

        self.ind_pinhole.sort()

        self.ind_bpm_pinhole = self.ind_bpms + self.ind_pinhole
        self.ind_bpm_pinhole.sort()

        # print(np.diff(self.ind_bpm_pinhole,1))
        # time.sleep(10)

        self.mask_bpm = [self.ind_bpm_pinhole.index(x) for x in self.ind_bpms]
        self.mask_pin = [self.ind_bpm_pinhole.index(x) for x in self.ind_pinhole]

        self.id_markers = list(get_refpts(self.ring, 'ID*'))

        # set RF and voltage to model one
        self.ligth_speed = 2.99792458e08
        self.Circumference = self.ring.s_range[-1]

        self.harm = self.ring[self.ind_rf[0]].HarmNumber

        self.rf_frequency = self.ring[self.ind_rf[0]].Frequency
        self.rf_voltage = sum([self.ring[f].Voltage for f in self.ind_rf])

        # print parameters
        self.log.debug('RF: {} Hz, {} V, {} bunches'.format(self.rf_frequency, self.rf_voltage, self.harm))
        self.log.debug('C: {} m'.format(self.Circumference))
        self.log.debug('# IDs: {} '.format(len(self.id_markers)))
        self.log.debug('# BPMs: {} '.format(len(self.bpm_position)))
        self.log.debug('# Pinholes: {} '.format(len(self.ind_pinhole)))

        if len(self.rel) is 0:
            self.log.debug('get BPM ERRORS')
            # compute BPM error matrices if not existing
            self.rel, self.tel, self.trand = bpm_modifications.bpm_matrices(self.ring, self.ind_bpms)
            self.log.debug('get BPM ERRORS done')

        # compute/store some other usefull lattice related quantities
        self.delta_l = ((self.ligth_speed * self.harm) / self.rf_frequency) - self.Circumference

        # define list of device names to monitor for change
        device_list = []
        for icell in range(1, 33, 1):

            # create a list of individual devices
            device_list = device_list + \
                          ['srmag/hst-sh1/c{0:02d}-a'.format(icell)] + \
                          ['srmag/vst-sh1/c{0:02d}-a'.format(icell)] + \
                          ['srmag/sqp-sh1/c{0:02d}-a'.format(icell)] + \
                          ['srmag/m-qf1/c{0:02d}-a'.format(icell)] + \
                          ['srmag/m-qd2/c{0:02d}-a'.format(icell)] + \
                          ['srmag/m-qd3/c{0:02d}-a'.format(icell)] + \
                          ['srmag/m-sd1/c{0:02d}-a'.format(icell)] + \
                          ['srmag/hst-sd1/c{0:02d}-a'.format(icell)] + \
                          ['srmag/vst-sd1/c{0:02d}-a'.format(icell)] + \
                          ['srmag/sqp-sd1/c{0:02d}-a'.format(icell)] + \
                          ['srmag/m-qf4/c{0:02d}-a'.format(icell)] + \
                          ['srmag/m-sf2/c{0:02d}-a'.format(icell)] + \
                          ['srmag/hst-sf2/c{0:02d}-a'.format(icell)] + \
                          ['srmag/vst-sf2/c{0:02d}-a'.format(icell)] + \
                          ['srmag/sqp-sf2/c{0:02d}-a'.format(icell)] + \
                          ['srmag/m-qf4/c{0:02d}-b'.format(icell)] + \
                          ['srmag/m-of1/c{0:02d}-b'.format(icell)] + \
                          ['srmag/m-sd1/c{0:02d}-b'.format(icell)] + \
                          ['srmag/hst-sd1/c{0:02d}-b'.format(icell)] + \
                          ['srmag/vst-sd1/c{0:02d}-b'.format(icell)] + \
                          ['srmag/sqp-sd1/c{0:02d}-b'.format(icell)] + \
                          ['srmag/m-qd5/c{0:02d}-b'.format(icell)] + \
                          ['srmag/m-qf6/c{0:02d}-b'.format(icell)] + \
                          ['srmag/m-dq1/c{0:02d}-b'.format(icell)] + \
                          ['srmag/hst-dq1/c{0:02d}-b'.format(icell)] + \
                          ['srmag/m-qf8/c{0:02d}-b'.format(icell)] + \
                          ['srmag/hst-sh2/c{0:02d}-b'.format(icell)] + \
                          ['srmag/vst-sh2/c{0:02d}-b'.format(icell)] + \
                          ['srmag/sqp-sh2/c{0:02d}-b'.format(icell)] + \
                          ['srmag/m-dq2/c{0:02d}-c'.format(icell)] + \
                          ['srmag/hst-dq2/c{0:02d}-c'.format(icell)] + \
                          ['srmag/m-qf8/c{0:02d}-d'.format(icell)] + \
                          ['srmag/m-dq1/c{0:02d}-d'.format(icell)] + \
                          ['srmag/hst-dq1/c{0:02d}-d'.format(icell)] + \
                          ['srmag/m-qf6/c{0:02d}-d'.format(icell)] + \
                          ['srmag/m-qd5/c{0:02d}-d'.format(icell)] + \
                          ['srmag/m-sd1/c{0:02d}-d'.format(icell)] + \
                          ['srmag/hst-sd1/c{0:02d}-d'.format(icell)] + \
                          ['srmag/vst-sd1/c{0:02d}-d'.format(icell)] + \
                          ['srmag/sqp-sd1/c{0:02d}-d'.format(icell)] + \
                          ['srmag/m-of1/c{0:02d}-d'.format(icell)] + \
                          ['srmag/m-qf4/c{0:02d}-d'.format(icell)] + \
                          ['srmag/m-sf2/c{0:02d}-e'.format(icell)] + \
                          ['srmag/hst-sf2/c{0:02d}-e'.format(icell)] + \
                          ['srmag/vst-sf2/c{0:02d}-e'.format(icell)] + \
                          ['srmag/sqp-sf2/c{0:02d}-e'.format(icell)] + \
                          ['srmag/m-qf4/c{0:02d}-e'.format(icell)] + \
                          ['srmag/m-sd1/c{0:02d}-e'.format(icell)] + \
                          ['srmag/hst-sd1/c{0:02d}-e'.format(icell)] + \
                          ['srmag/vst-sd1/c{0:02d}-e'.format(icell)] + \
                          ['srmag/sqp-sd1/c{0:02d}-e'.format(icell)] + \
                          ['srmag/m-qd3/c{0:02d}-e'.format(icell)] + \
                          ['srmag/m-qd2/c{0:02d}-e'.format(icell)] + \
                          ['srmag/m-qf1/c{0:02d}-e'.format(icell)] + \
                          ['srmag/hst-sh3/c{0:02d}-e'.format(icell)] + \
                          ['srmag/vst-sh3/c{0:02d}-e'.format(icell)] + \
                          ['srmag/sqp-sh3/c{0:02d}-e'.format(icell)]

        # # debug line to test with only a few devices
        # print('DEBUG LINE: REMOVE FOR REAL TEST. LIMITES monitored devices to few to speed up.')
        # device_list = []

        # add special devices to list
        device_list = device_list + \
                      ['srmag/m-qf2/c04-a'] + \
                      ['srmag/m-qf2/c03-e'] + \
                      ['srmag/m-mb-qf1/c30-e'] + \
                      ['srmag/m-mb-qd2/c30-e'] + \
                      ['srmag/m-mb-qd2/c31-a'] + \
                      ['srmag/m-mb-qf1/c31-a'] + \
                      ['srmag/sext-sh3/c03-e'] + \
                      ['srmag/sext-sh1/c04-a']  # those are in injection application, add later

        # dictionary of device-names / indexes in AT lattice
        self.dev_ind_dict = dict(zip(device_list, [None] * len(device_list)))

        # dictionary of device-names / event ID
        self.dev_event_id_dict = dict(zip(device_list, [None] * len(device_list)))

        # dictionary of device-names / strength in AT lattice
        self.dev_strength_model_dict = dict(zip(device_list, [None] * len(device_list)))

        # dictionary of device-names / errors strength in AT lattice (computed using ring_err and ring)
        self.dev_strength_err_model_dict = dict(zip(device_list, [None] * len(device_list)))

        # dictionary of device-names / correction strength in AT lattice (computed using ring_err and ring_cor)
        self.dev_strength_cor_model_dict = dict(zip(device_list, [None] * len(device_list)))

        # dictionary of device-name / AT lengths
        self.dev_length_dict = dict(zip(device_list, [None] * len(device_list)))

        # dictionary of device-names / strength in machine
        self.dev_strength_machine_dict = dict(zip(device_list, [None] * len(device_list)))

        # dictionary of device-names / tango attributes
        self.dev_attribute_dict = dict(zip(device_list, [None] * len(device_list)))

        # dictionary of device-frozen state / tango attributes, to be used by ResetSimulator
        self.dev_frozen_dict = dict(zip(device_list, [None] * len(device_list)))

        # initialize model dictionary
        self.init_dict_model()

        for k, v in self.dev_ind_dict.items():
            if v:
                self.log.debug('{} has index {}, and is composed of: {} elements'.format(k, v, len(v)))
            else:
                self.log.debug('{} DOES NOT EXIST'.format(k, v))

        # update optics attributes according to the requested mode for the reference lattice with errors and corrections
        self.update_data()

        # set modified flags
        self.magnet_is_modified = False
        self.rf_is_modified = False

        # minimum K change accepted
        self.change_threshold = 1e-9

        # list of modified keys
        self.modified_keys = []

        os.environ['TANGO_HOST'] = self.TangoHost

        present_host = os.environ['TANGO_HOST']
        self.log.info('TANGO_HOST at startup is : {}'.format(present_host))
        if present_host not in self.TangoHost:
            self.log.info(present_host)
            self.log.info(self.TangoHost)
            self.log.info(present_host is self.TangoHost)
            msg = "Tango Host " + present_host + " is not: " + self.TangoHost
            logging.error(msg)
            raise Exception(msg)

        # Check if present magnets setting are different from model and initialize attributes of devices to monitor
        # self.machine_is_modified = self.check_ring_modified()
        

        # self.check_mag_thread   = Thread(name='check_magnets', target = self.run_check_ring_modified, daemon=True)
        # self.update_ring_thread = Thread(name='update_ring', target=self.run_update_ring_data, daemon=True)
        # self.update_data_thread = Thread(name='update_data',   target = self.run_updatedata ,         daemon=True)

        # # append to list of running timers to cancel upon OFF command
        # self.runningthreads = []
        # self.runningthreads.append(self.check_mag_thread)
        # self.runningthreads.append(self.update_ring_thread)
        # self.runningthreads.append(self.update_data_thread)
    pass

    def init_dict_model(self):
        """
        initialize dictionaries with strengths , lengths and indexes.
        
        dictionary keys are the device names
        
        several dictionaries are available:
        dev_ind_dict  : lattice indexes (may be more than a single index per key)
        dev_length_dict[k] = Tot_L (total length of corresponding indexes in dev_ind_dict)
        dev_strength_model_dict[k] = K_cor * Tot_L              # gradients in the reference optics model
        dev_strength_err_model_dict[k] = (K_err - K) * Tot_L  # errors to be applied during simulation not visible by the user
        dev_strength_cor_model_dict[k] = (K_cor - K_err) * Tot_L

        K = gradient in reference lattice
        K_err = gradient with error
        K_cor = gradient with errors and correction  <- the model simulated

        DK_err = K_err - K
        DK_cor = K_cor - K_err

        in the simualtor devices Strength = K + DK_cor
        in the simualator lattice model Strength = K + DK_err + DK_cor  (as in real life, errors are invisible to the user)

        :return:
        """

        self.log.debug("preparing lattice model dictionary")

        list_of_not_available_devices = []

        # fill in dictionaries
        for k, v in self.dev_ind_dict.items():
            # assign index to dictionary
            matching_ind = []
            k0 = 0.0     # gradient in reference lattice
            k_err = 0.0  # error
            k_cor = 0.0  # correction

            # loop ring to find device name
            for el_cor, el_err, el in zip(self.ring, self.ring_err, self.ring_0):
                if hasattr(el_cor, "Device"):
                    search_k = k
                    if 'vst-sf' in k:
                        search_k = k.replace('vst-sf', 'm-sf')
                    elif 'hst-sf' in k:
                        search_k = k.replace('hst-sf', 'm-sf')
                    elif 'sqp-sf' in k:
                        search_k = k.replace('sqp-sf', 'm-sf')
                    elif 'vst-sd' in k:
                        search_k = k.replace('vst-sd', 'm-sd')
                    elif 'hst-sd' in k:
                        search_k = k.replace('hst-sd', 'm-sd')
                    elif 'sqp-sd' in k:
                        search_k = k.replace('sqp-sd', 'm-sd')
                    elif 'hst-dq' in k:
                        search_k = k.replace('hst-dq', 'm-dq')
                    elif 'hst' in k:
                        search_k = k.replace('hst', 'vst')
                    elif 'sqp' in k:
                        search_k = k.replace('sqp', 'vst')
                    elif 'sext' in k:
                        search_k = k.replace('sext', 'vst')

                    # search this device name
                    if search_k in el_cor.Device:  # match full device name
                        # get index
                        matching_ind.append(self.ring.index(el_cor))
                        # get gradient (of last found)
                        if "m-q" in k:
                            k0 = el.PolynomB[1]         # no error no correction
                            k_err = el_err.PolynomB[1]  # error no correction
                            k_cor = el_cor.PolynomB[1]  # errors and corrections

                        elif "m-mb-q" in k:
                            k0 = el.PolynomB[1]  # no error no correction
                            k_err = el_err.PolynomB[1]  # error no correction
                            k_cor = el_cor.PolynomB[1]  # errors and corrections

                        elif "m-dq" in k:
                            k0 = el.PolynomB[1]
                            k_err = el_err.PolynomB[1]
                            k_cor = el_cor.PolynomB[1]

                        elif "m-s" in k:
                            k0 = el.PolynomB[2]
                            k_err = el_err.PolynomB[2]
                            k_cor = el_cor.PolynomB[2]

                        elif "sext" in k:

                            k0 = el.PolynomB[2]
                            k_err = el_err.PolynomB[2]
                            k_cor = el_cor.PolynomB[2]

                        elif 'm-o' in k:
                            k0 = el.PolynomB[3]
                            k_err = el_err.PolynomB[3]
                            k_cor = el_cor.PolynomB[3]

                        elif 'hst' in k:
                            # sum KickAngle and PolynomB(1)
                            k0 = 0.0
                            k_err = 0.0
                            k_cor = 0.0
                            try:
                                k0 = el.KickAngle[0] / el.Length
                                k_err = el_err.KickAngle[0] / el.Length
                                k_cor = el_err.KickAngle[0] / el.Length
                            except:
                                pass

                            k0 = - el.PolynomB[0] + k0
                            k_err = - el_err.PolynomB[0] + k_err
                            k_cor = - el_cor.PolynomB[0] + k_cor

                        elif 'vst' in k:
                            # sum KickAngle and PolynomA(1)
                            k0 = 0.0
                            k_err = 0.0
                            k_cor = 0.0
                            try:
                                k0 = el.KickAngle[1] / el.Length
                                k_err = el_err.KickAngle[1] / el.Length
                                k_cor = el_err.KickAngle[1] / el.Length
                            except:
                                pass

                            k0 = el.PolynomA[0] + k0
                            k_err = el_err.PolynomA[0] + k_err
                            k_cor = el_cor.PolynomA[0] + k_cor

                        elif 'sqp' in k:
                            k0 = el.PolynomA[1]
                            k_err = el_err.PolynomA[1]
                            k_cor = el_cor.PolynomA[1]

                        # # break # if found, break for loop: DO NOT DO THIS: WILL SKIP SLICED ELEMENTS

            self.log.debug(matching_ind)

            if len(matching_ind) is 0:
                self.log.debug("no indexes for {}. removed from list of controlled devices".format(k))
                list_of_not_available_devices.append(k)
                print(list_of_not_available_devices)
            else:
                # assign strength to dictionary

                # length
                tot_l = sum([x.Length for x in self.ring[matching_ind]])
                self.dev_length_dict[k] = tot_l

                # indexes
                self.dev_ind_dict[k] = matching_ind

                # gradients in the reference optics model with errors and corrections
                self.dev_strength_model_dict[k] = k_cor * tot_l

                # errors to be applied during simulation not visible by the user
                self.dev_strength_err_model_dict[k] = (k_err - k0) * tot_l

                # correction to be applied use for ResetSimulator
                self.dev_strength_cor_model_dict[k] = (k_cor - k_err) * tot_l

                self.log.debug("{} made of {} slices has length {} index {} and gradient {}".format(
                        k,
                        len(matching_ind),
                        tot_l,
                        self.dev_ind_dict[k],
                        self.dev_strength_model_dict[k]
                    ))

        # remove not found devices
        for k in list_of_not_available_devices:
            try:
                self.dev_length_dict.pop(k, 'None')
                self.dev_ind_dict.pop(k, 'None')
                self.dev_strength_model_dict.pop(k, 'None')
                self.dev_strength_err_model_dict.pop(k, 'None')
                self.dev_strength_cor_model_dict.pop(k, 'None')
                self.dev_strength_machine_dict.pop(k, 'None')
                self.log.info("deleted {} from list of controlled devices".format(k))
            except Exception:
                self.log.debug('delete {} impossible'.format(k))

        if self.verbose:
            print("There are {} devices to monitor".format(len(self.dev_ind_dict)))

    def set_strengths_to_simulator(self):
        """
        sets strengths from model to machine
        used by ResetSimulator
        sets K + K_cor to Strength

        :return:
        """

        self.log.debug('---- ---- ----')
        self.log.debug('Start: Set strengths to simulator')

        # verify tango host and do not act in case it is real SR
        not_allowed_hosts = ['acs.esrf.fr:10000', 'acs.esrf.fr:11000', 'acs.esrf.fr:10000,acs.esrf.fr:11000']
        present_host = os.environ['TANGO_HOST']

        self.log.info(present_host)
        self.log.debug('---- ---- ----')

        if present_host in not_allowed_hosts:
            raise Exception('Tango Host is ' + present_host + ' not allowed for this action')
        else:
            self.log.debug('Setting model strengths in simulator: ' + present_host)

        # if not existing, initialize Attributes dictionary
        A = "Strength"
        F = "Frozen"
        for k, v in self.dev_strength_machine_dict.items():
            # loop attributes to check
            if self.dev_attribute_dict[k] is None:
                # define attribute if not existing already
                self.dev_attribute_dict[k] = Attribute(self.TangoHost + "/" + k + "/" + A)
                self.dev_frozen_dict[k] = Attribute(self.TangoHost + "/" + k + "/" + F)

                self.log.debug("{} defined in attribute dictionary".format(k))
                # time.sleep(10)
            # else:
            # print("{} already present in dictionary".format(k))

            self.log.debug("{} {} is {}".format(k, A, self.dev_attribute_dict[k].value))
            self.dev_strength_machine_dict[k] = self.dev_attribute_dict[k].value

        # in case, update a vector of modified values.
        for k, v in self.dev_strength_model_dict.items():

            frozen_state = self.dev_frozen_dict[k].value
            # print('{} frozen state {}'.format(k, frozen_state))
            if frozen_state:
                self.log.debug('un-freeze {}'.format(k))
                self.dev_frozen_dict[k].write(False)  # un-freeze

            # set attribute value to model one (integrated strengths K+DK_cor)
            # dev_strength_model_dict[k]     = K + DKcor + DKerr
            # dev_strength_machine_dict[k]   = K + DKcor
            # dev_strength_err_model_dict[k] = DKerr
            # dev_strength_cor_model_dict[k] = DKcor

            #try:
            self.log.debug('present k for {} is {}'.format(k, self.dev_attribute_dict[k].value))

            self.log.debug('setting {} to model value: {}'.format(k, self.dev_strength_model_dict[k] - self.dev_strength_err_model_dict[k]))
            self.dev_attribute_dict[k].write(
                self.dev_strength_model_dict[k] - self.dev_strength_err_model_dict[k]
                )
            # wait attribute set reach
            count = 0
            while abs(self.dev_attribute_dict[k].value -
                       (self.dev_strength_model_dict[k] - self.dev_strength_err_model_dict[k] )
                      ) > 1e-4 and count<100:
                self.log.info('waiting for {} to reach setting'.format(k))
                time.sleep(0.01)
                count += 1

            #except Exception as ex:
            #    print('impossible to set: {}'.format(k))
            #    print(ex)

            # time.sleep(0.1)

            if frozen_state:
                self.log.debug('re-freeze {}'.format(k))
                self.dev_frozen_dict[k].write(frozen_state)  # re freeze as original

            #if self.verbose:
            self.log.debug("{} has been modified from {:10.6f} to {:10.6f} - {:10.6f}".format(
                    k,
                    self.dev_strength_machine_dict[k],
                    self.dev_strength_model_dict[k],
                    self.dev_strength_err_model_dict[k],
                ))

        self.log.debug('---- ---- ----')
        self.log.debug('END: Set strengths to simulator')
        self.log.debug('---- ---- ----')

    def get_modified_strengths(self, strengths, devnames):
        """
        translate the output of GetUpdStrengths to use by this class
        INPUTS:
        strengths: list of strengths
        devnames: list of device name

        len(strengths) must be equal to len(devnames)

        """

        # intialize modified keys
        self.modified_keys = []

        for k, s in zip(devnames, strengths):

            if k in self.dev_strength_machine_dict:
                # self.log.info("{} is in dev_strength_machine_dict".format(k))
                self.dev_strength_machine_dict[k] = s
                self.modified_keys.append(k)

                D = self.dev_strength_machine_dict[k] - \
                    (self.dev_strength_model_dict[k] - self.dev_strength_err_model_dict[k])

                self.log.debug("{} has been modified from {:10.4f} to {:10.4f} (delta = {:10.4f}) ".format(
                        k,
                        self.dev_strength_model_dict[k] - self.dev_strength_err_model_dict[k],
                        self.dev_strength_machine_dict[k],
                        D
                    ))
            else:
                self.log.info("{} is not in the SR lattice".format(k))

        if len(self.modified_keys) > 0:
            self.magnet_is_modified = True
            self.log.debug("{} magnets are modified".format(len(self.modified_keys)))

        pass

    def check_ring_modified(self):
        """
        check if any magnet has been modified

        loops through every device listed to check if anything has changed.

        this function is NOT necessary when using ebs_simu.py, use instead get_modified_strengths

        """

        # intialize modified keys
        self.modified_keys = []

        A = "Strength"
        F = "Frozen"
        for k, v in self.dev_strength_machine_dict.items():
            # loop attributes to check
            if self.dev_attribute_dict[k] is None:
                # define attribute if not existing already
                self.dev_attribute_dict[k] = Attribute(self.TangoHost + "/" + k + "/" + A)
                self.dev_frozen_dict[k] = Attribute(self.TangoHost + "/" + k + "/" + F)
                self.log.debug("{} defined in attribute dictionary".format(k))
                # time.sleep(10)
            # else:
            # print("{} already present in dictionary".format(k))

            self.log.debug("{} {} is {}".format(k, A, self.dev_attribute_dict[k].value))
            self.dev_strength_machine_dict[k] = self.dev_attribute_dict[k].value

        # in case, send an event and a update a vector of modified values.
        for k, v in self.dev_strength_model_dict.items():
            # compute difference machine to model
            D = self.dev_strength_machine_dict[k] - \
                (self.dev_strength_model_dict[k] - self.dev_strength_err_model_dict[k])

            # if above specified level, add to list of modified keys
            if abs(D) > self.change_threshold:

                # list of modified keys
                self.modified_keys.append(k)

                self.log.debug("{} has been modified from {:10.4f} to {:10.4f} (delta = {:10.4f}) ".format(
                        k,
                        self.dev_strength_model_dict[k] - self.dev_strength_err_model_dict[k],
                        self.dev_strength_machine_dict[k],
                        D
                    ))

        if len(self.modified_keys) > 0:
            self.magnet_is_modified = True
            self.log.debug("{} magnets are modified".format(len(self.modified_keys)))

        return self.magnet_is_modified or self.rf_is_modified

    def update_ring(self):
        """
        modify ring lattice
        set strengths from machine to model and adds errors (unknown to the simulator user)

        :return:
        """

        # check if any modifications
        self.rf_is_modified = self.ring[self.ind_rf[0]].Frequency != self.rf_frequency
        self.log.debug(self.ring[self.ind_rf[0]].Frequency)
        self.log.debug(self.rf_frequency)

        lattice_is_modified = self.magnet_is_modified or self.rf_is_modified

        self.log.debug("There are {} devices to modify".format(len(self.modified_keys)))
        self.log.debug("rf changed: {}".format(self.rf_is_modified))

        if lattice_is_modified:

            self.log.info('magnet or RF modified')

            # turn to false the matlab file saved flag. if entering here, there is a change in the lattice
            self.mat_save_ring = False

            # set RF cavity parameters
            self.log.debug(self.ind_rf)

            ncav = len(self.ind_rf)
            for ir in self.ind_rf:
                self.log.debug("{0} freq set to {1} Volt set to {2}".format(self.ring[ir].FamName, self.rf_frequency,
                                                                       self.rf_voltage / ncav))
                self.ring[ir].Frequency = self.rf_frequency
                self.ring[ir].Voltage = self.rf_voltage / ncav
            # compute delta_l for off-energy computations

            self.delta_l = ((self.ligth_speed * self.harm) / self.rf_frequency) - self.Circumference

            self.log.debug('delta L: {} '.format(self.delta_l))
            # print(self.modified_keys)

            # apply modifications
            for k in self.modified_keys:
                # get indexes from dictionary

                # print(k) # key = device name
                ind = self.dev_ind_dict[k]  # index list
                # print(ind)
                el = self.ring[ind[0]]  # first corresponding element in lattice
                # print(el)
                DGerr = self.dev_strength_err_model_dict[k]  # present value
                # print(DGerr)

                # gradient ERRORS added here as red from lattice files, to be set in the model
                G = self.dev_strength_machine_dict[k] + DGerr   # desired value machine (K+Kcor) + error (K_err)

                # print(G)
                G0 = self.dev_strength_model_dict[k]  # present value  (K + K_err + K_cor)

                # print(G0)
                L = self.dev_length_dict[k]  # length of element (needed for sliced elements)

                self.log.debug("{} {} {} is being modified from {:10.4f} to {:10.4f}".format(
                        k,
                        ind[0],
                        el.FamName,
                        G0,
                        G 
                    ))

                # print(ind)

                for ii in list(ind):  # set gradient (of last found)

                    self.log.debug("modify ring index {}. One of {} slices composing magnet {}".format(ii, len(ind), k))

                    Lpb = len(self.ring[ii].PolynomB)

                    if "m-q" in k:
                        self.ring[ii].PolynomB[1] = G / L
                    elif "m-mb-q" in k:
                        self.ring[ii].PolynomB[1] = G / L
                    elif "m-dq" in k:
                        self.ring[ii].PolynomB[1] = G / L
                    elif "m-s" in k:
                        self.ring[ii].PolynomB[2] = G / L
                    elif "sext" in k:
                        self.ring[ii].PolynomB[2] = G / L
                    elif 'm-o' in k:
                        self.ring[ii].PolynomB[3] = G / L
                    elif 'hst' in k:
                        try:
                            self.ring[ii].KickAngle[0] = 0  # all on PolynomB[0]
                        except:
                            pass

                        self.ring[ii].PolynomB[0] = - G / L

                    elif 'vst' in k:
                        try:
                            self.ring[ii].KickAngle[1] = 0  # all on PolynomA[0]
                        except:
                            pass

                        self.ring[ii].PolynomA[0] = G / L

                    elif 'sqp' in k:
                        self.ring[ii].PolynomA[1] = G / L

                # print(el)

                # replace in ring with new element
                # self.ring[ind] = el
                # update dict strength model
                self.dev_strength_model_dict[k] = self.dev_strength_machine_dict[k]

            # reset modified flags:
            self.magnet_is_modified = False
            self.rf_is_modified = False

            self.log.debug('ring has been updated')
            self.log.debug('---------------------')

        else:
            self.log.debug('no magnet was modified')
            # time.sleep(1)

        # add magnet vibration and amplitude errors to all quadrupoles
        # use self.quad_vib_ampl

        for k in self.dev_ind_dict:
            ind = self.dev_ind_dict[k]  # index list
            # print(ind)
            # el = self.ring[ind[0]]  # first corresponding element in lattice
            # L = self.dev_length_dict[k]  # length of element (needed for sliced elements)
            for ii in list(ind):  # set gradient (of last found)

                if "m-q" in k:

                    self.log.debug(" random vibration for ind {}, slice #{} of {}".format(ii, len(ind), k))

                    # vibration
                    if 0 < self.quad_vib_ampl < 1e-5:
                        self.log.debug('setting quadrupole vibrations errors')

                        DGvib = self.quad_vib_ampl * np.random.randn(2)
                        # print(hasattr(self.ring[ii], 'T1'))
                        # at.set_shift(self.ring,0.0,0.0)
                        # print(hasattr(self.ring[ii], 'T1'))

                        if not hasattr(self.ring[ii], 'T1'):
                            self.ring[ii].T1 = np.array([0.0]*6)
                            self.ring[ii].T2 = np.array([0.0]*6)

                        if not hasattr(self.ring_err[ii], 'T1'):
                            self.ring_err[ii].T1 = np.array([0.0] * 6)
                            self.ring_err[ii].T2 = np.array([0.0] * 6)

                        self.ring[ii].T1[0] = self.ring_err[ii].T1[0] - DGvib[0]
                        self.ring[ii].T1[2] = self.ring_err[ii].T1[2] - DGvib[1]
                        self.ring[ii].T2[0] = self.ring_err[ii].T2[0] + DGvib[0]
                        self.ring[ii].T2[2] = self.ring_err[ii].T2[2] + DGvib[1]

                    else:
                        self.log.debug('no vibrations')

                    # jitter
                    if 0 < self.quad_grad_jit < 1e-1:
                        self.log.debug('setting random quadrupole gradient jitter errors {:02f}%'.format(self.quad_grad_jit*100))

                        DGjit = self.quad_grad_jit * np.random.randn(1)
                        self.ring[ii].PolynomB[1] = self.dev_strength_model_dict[k] / \
                                                    self.dev_length_dict[k] * (1 + DGjit) # in any case PolynomB[1] is assigned according to dev_strength_dict

                    else:
                        self.log.debug('no jitter errors (less then zero or more than 0.1 1/m2)')

        pass

    def comp_emit_atx(self, orb):
        # compute emittances for ring
        turn_rad_off = False

        if not self.rad_on:
            self.ring.radiation_on()
            turn_rad_off = True

        emit0, bbb, eee = self.ring.ohmi_envelope(orbit=orb.T, refpts=self.ind_pinhole)
        # env = self.ring.envelope_parameters()
        # env.emittances[0]

        # get global lattice parameters
        self.emittance_h = emit0['emitXY'][0]
        self.emittance_v = emit0['emitXY'][1]

        for ip in range(0, len(self.ind_pinhole)):
            self.sigmah_pin[ip] = eee['r66'][ip][0][0]
            self.sigmav_pin[ip] = eee['r66'][ip][2][2]
            self.sigmahv_pin[ip] = eee['r66'][ip][2][0]

        self.log.debug(emit0['emitXY'])

        self.detailed_atx_data = ((0.0, 0.0, 0.0, 0.0, 0.0),)

        if turn_rad_off:
            self.ring.radiation_off()

        # else:
        #    self.log.debug('Radiation is OFF, not possible to compute emittance')

    def update_data(self):
        """
        compute data from lattice according to required mode

        modes are set via the variable mode
        ATX computes emittances
        TbT turn by Turn data

        BPM errors are added (as retrived from ring) to:
         closed orbit
         TbT data

        :return: data, tbt_data
        """

        start_time = time.time()
        self.simulation_started = True

        if self.rad_on:  # turn on radiation if it is not on.
            if not self.ring.radiation:
                self.ring.radiation_on()

        # # compute only if needed: does not apply in case of random noise errors
        # if not(self.magnet_is_modified or self.rf_is_modified):
        #    print('nothing changes, not computing optics')
        #    time.sleep(1)
        #    return

        self.log.debug('computing optics')

        t2d = np.zeros((2, int(self.tbt__buffer_size), len(self.ind_bpms)))
        t2d[:] = np.nan
        tt = t2d.flatten()

        # initialize relevant parameters
        if self.mode != Mode.Mixed:
            self.emittance_h = np.NaN
            self.emittance_v = np.NaN
        self.tune_h = np.NaN
        self.tune_v = np.NaN
        self.chromaticity_h = np.NaN
        self.chromaticity_v = np.NaN

        nb = len(self.ind_bpms)

        # how to apply BPMs errors? findorbitErr like method?
        self.closed_orbit_h = [np.NaN] * nb  # None or [] not accepted by pyTango
        self.closed_orbit_v = [np.NaN] * nb  # None or [] not accepted by pyTango

        self.beta_h = [np.NaN] * nb  # None or [] not accepted by pyTango
        self.beta_v = [np.NaN] * nb  # None or [] not accepted by pyTango
        self.eta_h = [np.NaN] * nb  # None or [] not accepted by pyTango
        self.eta_v = [np.NaN] * nb  # None or [] not accepted by pyTango

        # initialize TbT 2D and 3D arrays
        logging.debug("tbt nan default tb_t")
        nanmatrix = np.empty((int(nb), int(self.tbt__buffer_size),))
        nanmatrix[:] = np.nan
        self.h_positions_tb_t = nanmatrix
        self.v_positions_tb_t = nanmatrix
        del nanmatrix

        logging.debug("try to compute optics")
        optics_computation_ok = True

        try:  # try to compute optics

            self.log.debug('delta L: {} '.format(self.delta_l))
            try:  # try to get closed orbit
                # off energy closed orbit for computations
                if not self.rad_on:
                    self.log.debug('no radiation')
                    self.ring.radiation_off()
                    _orb_, orb = at.find_sync_orbit(self.ring, dct=self.delta_l, refpts=0)
                else:
                    _orb_, orb = at.find_orbit6(self.ring, refpts=0)

                self.log.debug('Orbit at start, tunes, chromaticity')
                self.log.debug(orb)
            except Exception:
                self.log.debug('Orbit computation failed')

            try:  # try to print tune
                self.log.debug(self.ring.get_tune())
            except Exception:
                self.log.debug('could not get tune')

            try:  # try to print chroma
                self.log.debug(self.ring.get_chrom())
            except Exception:
                self.log.debug('could not get chromaticity')

            # emittances from Ohmi Envelope

            if self.mode == Mode.Atx:
                self.log.debug("%s : emittances computed" % self.mode)
                self.comp_emit_atx(orb)

                # update ATX counter
                self.detailed_atx_counter += 1

            else:
                self.log.debug("%s : emittances not computed" % self.mode)

            # emittances from Ohmi Envelope only every given number of iterations
            if self.mode == Mode.Mixed:
                if not self.counter % self.atx_every:
                    self.log.debug("%s : emittances computed" % self.mode)
                    if self.rad_on:
                        self.comp_emit_atx(orb)
                    else:
                        self.log.debug('Radiation is OFF, not possible to compute emittance')

                    # update ATX counter
                    self.detailed_atx_counter += 1

            else:
                self.log.debug("%s : emittances not computed" % self.mode)

            # Turn by turn
            if self.mode == Mode.TbT:

                # self.ring.radiation_on()

                self.log.info('start at {} for {} turns'.format(
                        self.tbt__in_coord,
                        self.tbt__buffer_size,
                    ))

                # print(self.ind_bpms)
                try:
                    coord = at.track.lattice_pass(
                        self.ring,
                        np.array(list(self.tbt__in_coord)),
                        nturns=int(self.tbt__buffer_size),
                        refpts=self.ind_bpms
                    )
                except Exception as e:
                    coord = np.array(list(self.tbt__in_coord))
                    print(e)

                # APPLY BPM ERRORS
                h_coord_err = coord[0, 0, 0:, 0:]
                v_coord_err = coord[2, 0, 0:, 0:]

                # apply BPM noise (random each turn) and BPM errors (same at each turn)
                for iturn in range(0, int(self.tbt__buffer_size), 1):

                    self.log.debug('set BPM ERRORS and noise')
                    trand = [t + self.bpm_noise * np.random.randn(1, 2)[0] for t in self.trand]

                    h_err, v_err = bpm_modifications.bpm_process(
                        coord[0, 0, 0:, iturn],
                        coord[2, 0, 0:, iturn],
                        self.rel, self.tel, trand)
                    t2d[0, iturn, 0:] = h_err
                    t2d[1, iturn, 0:] = v_err

                    h_coord_err[0:, iturn] = h_err
                    v_coord_err[0:, iturn] = v_err

                # turn by turn coordinates
                self.h_positions_tb_t = h_coord_err
                self.v_positions_tb_t = v_coord_err
                self.log.info('H tbt')
                self.log.info(h_coord_err)
                self.log.info(len(h_coord_err))

                # self.h_positions_tb_t = coord[0, 0, 0:, 0:]
                # self.v_positions_tb_t = coord[2, 0, 0:, 0:]

            else:  # compute optics for all modes, apart TbT mode
                self.log.debug("TbT data not computed")

                # compute optics only if not in TbT mode

                # turn off radiation to compute common optics parameters
                self.log.debug('at linear optics at dp/p = {} percent'.format(orb[0, 4] * 100))

                if not self.rad_on:
                    self.ring.radiation_off(quadrupole_pass='auto')

                    l0, t, c, l = self.ring.linopt(orb[0, 4], refpts=self.ind_bpm_pinhole, get_chrom=True)
                    self.tune_h = t[0]
                    self.tune_v = t[1]
                    self.chromaticity_h = c[0]
                    self.chromaticity_v = c[1]
                else:
                    l0, beamdata, l = self.ring.get_optics(refpts=self.ind_bpm_pinhole, get_chrom=True)
                    self.tune_h = beamdata.tune[0]  # t[0]
                    self.tune_v = beamdata.tune[1]  # t[1]
                    self.chromaticity_h = beamdata.chromaticity[0]  # c[0]
                    self.chromaticity_v = beamdata.chromaticity[1]  # c[1]


                lp = l[self.mask_pin]
                lb = l[self.mask_bpm]

                # APPLY BPM ERRORS

                # apply BPM noise errors
                self.log.debug('set BPM ERRORS and noise')
                trand = [t + self.bpm_noise * np.random.randn(1, 2)[0] for t in self.trand]

                h_err, v_err = bpm_modifications.bpm_process(
                    lb['closed_orbit'][:, 0],
                    lb['closed_orbit'][:, 2],
                    self.rel, self.tel, trand)

                # how to apply BPMs errors? findorbitErr like method?
                self.closed_orbit_h = h_err
                self.closed_orbit_v = v_err

                self.closed_orbit_h_pin = lp['closed_orbit'][:, 0]
                self.closed_orbit_v_pin = lp['closed_orbit'][:, 2]

                self.beta_h = lb['beta'][:, 0]
                self.beta_v = lb['beta'][:, 1]
                self.eta_h = lb['dispersion'][:, 0]
                self.eta_v = lb['dispersion'][:, 2]

        except Exception as exc:
            print(exc)
            logging.error('failed to compute optics: save lattice ')

            # self.save_ring(filename=os.path.join(os.path.dirname(self.logfile), 'failed_optics_lattice'))
            # time.sleep(20)
            optics_computation_ok = False
            logging.error('failed to compute optics')
            # raise OpticsComputationError('failed to compute optics')

        self.simulation_started = False
        # self.loop_time = time.time() - start_time
        self.counter = self.counter + 1

        self.log.debug("ring: %s" % self.ring_name)
        self.log.debug("mode: %s" % self.mode)
        self.log.debug("ATX every: %s" % self.atx_every)
        self.log.debug("bpm noise: %s" % self.bpm_noise)
        self.log.debug("quad jitter: %s " % self.quad_grad_jit)
        self.log.debug("quad vib: %s" % self.quad_vib_ampl)

        nbpm = len(self.ind_bpms)
        #print('h or: {}'.format(len(self.closed_orbit_h)))
        #print('v or: {}'.format(len(self.closed_orbit_v)))
        #print('h d: {}'.format(len(self.eta_h)))
        #print('v d: {}'.format(len(self.eta_v)))
        #print('h b: {}'.format(len(self.beta_h)))
        #print('v b: {}'.format(len(self.beta_v)))

        # this has to be the same as in RingSimulator.PushRawData
        data = [1.0] * 1936
        data[0] = self.tune_h
        data[1] = self.tune_v
        data[2] = self.chromaticity_h
        data[3] = self.chromaticity_v
        data[4] = self.emittance_h
        data[5] = self.emittance_v

        start = 6
        end = start + nbpm
        data[start:end] = self.closed_orbit_h

        start = end
        end = start + nbpm
        data[start:end] = self.closed_orbit_v

        start = end
        end = start + nbpm
        data[start:end] = self.beta_h

        start = end
        end = start + nbpm
        data[start:end] = self.beta_v

        start = end
        end = start + nbpm
        data[start:end] = self.eta_h

        start = end
        end = start + nbpm
        data[start:end] = self.eta_v

        start = end
        end = start + 5
        data[start:end] = self.closed_orbit_h_pin  # hor orbit at pinholes

        start = end
        end = start + 5
        data[start:end] = self.closed_orbit_v_pin  # ver orbit at pinholes

        start = end
        end = start + 5
        data[start:end] = self.sigmah_pin  # sig hor at pinholes

        start = end
        end = start + 5
        data[start:end] = self.sigmav_pin  # sig ver at pinholes

        start = end
        end = start + 5
        data[start:end] = self.sigmahv_pin  # sighv at pinholes

        #print(self.closed_orbit_h_pin)
        #print(self.closed_orbit_v_pin)
        self.log.debug('sigma h')
        self.log.debug(self.sigmah_pin)
        self.log.debug('sigma v')
        self.log.debug(self.sigmav_pin)
        self.log.debug('sigma hv')
        self.log.debug(self.sigmahv_pin)

        # TbT data
        tt = t2d.flatten()
        #print('length ot tbt  {}'.format(len(tt)))
        tbt_data = np.empty(len(tt)+1, dtype=object)
        #print('length ot tbt data {}'.format(len(tbt_data)))
        tbt_data[0] = int(self.tbt__buffer_size)
        tbt_data[1:len(tt)+1] = tt

        self.log.debug('tbt buffer size {}'.format(tbt_data[0]))
        self.log.debug('tbt first data {}'.format(tbt_data[0:9]))
        self.log.debug('tbt first data {}'.format(self.h_positions_tb_t[1:9]))
        self.log.debug('tbt last data {}'.format(tbt_data[-9:-1]))
        self.log.debug('tbt last data {}'.format(self.v_positions_tb_t[-9:-1]))

        return data, tbt_data.tolist(), optics_computation_ok

    """
    def run_check_ring_modified(self):
        # loop check of ring magnets strengths
        if self.verbose:
            print('inside check ring modified loop')
        # loop forever:
        # while True:
        for num in range(1, 100, 1):
            if self.verbose:
                print(num)
            # if state is ON. if moving, off, fault,...
            if self.state is 'ON':
                # if not locked (by other threads as reset for example), keep checking if magnets changed
                if not self.lockrunning.locked():
                    self.machine_is_modified = self.check_ring_modified()
                else:
                    if self.verbose:
                        print('check mag: locked')
                    time.sleep(1)
            else:
                if self.verbose:
                    print('check mag: OFF')
                time.sleep(1)

        pass

    def run_update_ring_data(self):
        # loop update ring
        if self.verbose:
            print('inside check modified + update ring + update data loop')

        # loop forever:
        # while True:
        for num in range(1, 100, 1):
            if self.verbose:
                print(num)
            if self.state is 'ON':
                # if not locked (by other threads as reset for example), keep checking if magnets changed
                if not self.lockrunning.locked():
                    start_time = time.time()  # measure loop time
                    self.lockrunning.acquire()  # while ring is updated, do not do anything else
                    try:
                        if self.verbose:
                            print('check mag: ')
                        self.machine_is_modified = self.check_ring_modified()
                    except Exception:
                        if self.verbose:
                            print('check mag: failed')

                    try:
                        if self.verbose:
                            print('updated ring: ')
                        self.update_ring()
                    except Exception:
                        if self.verbose:
                            print('updated ring: failed')

                    try:
                        if self.verbose:
                            print('updated data: ')
                        self.updatedata()
                    except Exception:
                        if self.verbose:
                            print('updated data: failed')

                    self.lockrunning.release()  # while ring is updated, do not do anything else
                    self.loop_time = time.time() - start_time
                else:
                    if self.verbose:
                        print('updated ring: locked')
                    time.sleep(1)
            else:
                if self.verbose:
                    print('updated ring: Off')
                time.sleep(1)

        pass

    def run_updatedata(self):
        # loop update data
        if self.verbose:
            print('inside update data loop')

        # loop forever:
        # while True:
        for num in range(1, 10, 1):
            if self.verbose:
                print(num)
            if self.state is 'ON':
                # if not locked (by other threads as reset for example), keep checking if magnets changed
                if not self.lockrunning.locked():
                    # compute optics, adds random errors
                    self.lockrunning.acquire()  # while ring is updated, do not do anything else
                    self.updatedata()
                    self.lockrunning.release()  # while ring is updated, do not do anything else
                else:
                    if self.verbose:
                        print('updated data: locked')
                    time.sleep(1)
            else:
                if self.verbose:
                    print('update data: off')
                time.sleep(1)

        pass
    """

    def plot_data(self, keep_plot_for_seconds=1):
        """
        makes some figures to display the computed optics
        :return:
        """
        fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6), (ax7, ax8)) = plt.subplots(4, 2, figsize=(12, 6))

        fig.suptitle("Tune: {:10.4f},{:10.4f}\n Emit [pm]: {:10.4f},{:10.4f}\n "
                     "Chrom: {:10.4f},{:10.4f}\n counter: {:06} ".format(
            self.tune_h, self.tune_v, self.emittance_h*1e12, self.emittance_v*1e12,
            self.chromaticity_h, self.chromaticity_v, self.counter))

        ax1.plot(self.beta_h)
        ax1.set_ylabel('Hor beta')

        ax2.plot(self.beta_v)
        ax2.set_ylabel('Ver beta')

        ax3.plot(self.eta_h)
        ax3.set_ylabel('hor disp')

        ax4.plot(self.eta_v)
        ax4.set_ylabel('ver disp')

        ax5.plot(self.closed_orbit_h)
        ax5.set_ylabel('hor orbit')
        ax5.set_xlabel('BPM #')

        ax6.plot(self.closed_orbit_v)
        ax6.set_ylabel('ver orbit')
        ax6.set_xlabel('BPM #')

        if self.mode is Mode.TbT:
            ax7.imshow(self.h_positions_tb_t)
            ax7.set_ylabel('hor orbit')
            ax7.set_xlabel('turn #')
            ax7.set_aspect('auto')

            ax8.imshow(self.v_positions_tb_t)
            ax8.set_ylabel('ver orbit')
            ax8.set_xlabel('turn #')
            ax8.set_aspect('auto')

        # plt.show(block=True)
        print('wait {}'.format(keep_plot_for_seconds))
        plt.pause(keep_plot_for_seconds)

        #        time.sleep(keep_plot_for_seconds)

    def save_ring(self, filename='saved_lattice.m'):
        """
        Saves lattice presently used by simulator

        input arguments:
        filename = file name to save to local folder or
                full path and filename with extension .mat to save to specific location
                make sure permissions are correct for this action
                default : saved_lattice.mat
        :return:None
        """

        def save_ring(_ring, _filename):
            for ii in _ring:
                if hasattr(ii, 'Calibration_units'):
                    delattr(ii, 'Calibration_units')

            at.save_m(_ring, _filename + '.m')
            at.save_mat(_ring, _filename + '.mat')

            pass

        save_fun = partial(save_ring, self.ring, filename)
        t = Thread(target=save_fun, name='save_lattice', daemon=True)
        t.start()

        self.mat_save_ring = False  # switch to False to say lattice has been saved already.

        self.log.info('saved_lattice.m and .mat file have been creaeted in ' + filename)

        pass

    def reset_simulator(self):
        """
        sets all magnets strengths to the lattice model file values

        :return:None
        """

        verbose_state = self.verbose

        self.log.debug("Start Reset Simu")

        # init from lattice model
        self.log.info('Init lattice indexes and strengths from simulator DS lattice file')
        self.initialize(
            lat_path=self.path_to_lattice_model,
            lat_var=self.variable_in_lattice_file,
            lat_var_err=self.variable_in_lattice_file_errors,
            lat_var_cor=self.variable_in_lattice_file_corrections,
            verb=self.verbose
        )

        # set strengths
        self.log.info('Set Strengths from lattice to simulator devices')
        self.set_strengths_to_simulator()

        # set RF freq and volt to lattice values
        self.log.info('set RF frequency and voltage')
        self.rf_frequency = self.ring[self.ind_rf[0]].Frequency
        self.rf_voltage = sum([self.ring[ir].Voltage for ir in self.ind_rf])
        # these values will be set by ebs_simu.py as they are attributes of the device itself.

        self.verbose = verbose_state

        self.log.debug("End Reset Simu")

        pass

"""
def test_fun():
    # initialize class
    print('INIT')
    C=ComputeOptics()

    C.verbose = False

    # check if any magnet has been modified
    print('Check lattice vs simulator')
    C.check_ring_modified()

    # update lattice
    print('update lattice')
    C.update_ring()

    # check if any magnet has been modified
    print('Check lattice  vs simulator')
    C.check_ring_modified()

    # compute optics for given mode
    print('update data')

    data, tbt_data = C.update_data()
    print(C.mode)
    print(len(data))
    print(len(tbt_data))

    print('update data TbT')
    C.mode = Mode.TbT
    data2, tbt_data2 = C.update_data()
    print(C.mode)
    print(len(data2))
    print(len(tbt_data2))


    # print('Save lattice')
    # C.SaveRing('../conda_recipe/test_save_lattice.mat')

    # print('Run the class initialization')
    # C.Init()

    # print('Reset Simulator')
    # C.ResetSimulator()

    # plot data
    C.plot_data()

    plt.close('all')

    # loop 3 times
    for ii in range(0,3,1):
        print('loop {}'.format(ii))
        C.verbose = True
        C.bpm_noise = ii*1e-8
        C.quad_grad_jit = ii * 1e-6
        C.quad_vib_ampl = ii * 1e-6
        C.rf_frequency = C.rf_frequency + 20  # add some frequency shift
        C.check_ring_modified()
        C.update_ring()
        C.update_data()
        C.plot_data(keep_plot_for_seconds=3)
        plt.close('all')

    # loop 3 times
    C.mode = Mode.TbT
    C.tbt__buffer_size = 100
    for ii in range(0,3,1):
        print('loop {}'.format(ii))
        C.verbose = True
        C.bpm_noise = ii*1e-8
        C.quad_grad_jit = ii * 1e-6
        C.quad_vib_ampl = ii * 1e-6
        C.rf_frequency = C.rf_frequency - 20  # add some frequency shift
        C.check_ring_modified()
        C.update_ring()
        C.update_data()
        C.plot_data(keep_plot_for_seconds=3)
        plt.close('all')
"""


def main(args=None, **kwargs):
    """Main function of the Pyringsimulator module."""
    return


"""
if __name__ == '__main__':
    main()
    
"""
