from builtins import isinstance, hasattr

import at
import numpy as np


def bpm_matrices(ring,ind_bpms):
    # read BPM matrices from lattice
    rel = []
    tel = []
    trand = []
    t1 = [0.0, 0.0]
    r1 = [[0.0, 0.0], [0.0, 0.0]]
    tb = [0.0, 0.0]
    rb = [[0.0, 0.0], [0.0, 0.0]]
    t_rand = [0.0, 0.0]

    for ib in ind_bpms:

        if hasattr(ring[ib], 'T1'):
            ii = [0, 2]
            t1 = [ring[ib].T1[iii] for iii in ii]
        else:
            t1 = [0.0, 0.0]

        if hasattr(ring[ib], 'R1'):
            r1[0][0] = ring[ib].R1[0][0]
            r1[0][1] = ring[ib].R1[0][2]
            r1[1][0] = ring[ib].R1[2][0]
            r1[1][1] = ring[ib].R1[2][2]

        else:
            r1 = [[1.0, 0.0], [0.0, 1.0]]

        if hasattr(ring[ib], 'Rotation'):
            if isinstance(ring[ib].Rotation, np.ndarray) or \
                isinstance(ring[ib].Rotation, np.float64 or \
                isinstance(ring[ib].Rotation, np.uint8)):

                dimr = ring[ib].Rotation.size
            else:
                dimr = len(ring[ib].Rotation)

            if dimr > 1:
                rb[0][0] = np.cos(ring[ib].Rotation[0])
                rb[0][1] = np.sin(ring[ib].Rotation[0])
                rb[1][0] = -np.sin(ring[ib].Rotation[0])
                rb[1][1] = np.cos(ring[ib].Rotation[0])
            else:
                rb[0][0] = np.cos(ring[ib].Rotation)
                rb[0][1] = np.sin(ring[ib].Rotation)
                rb[1][0] = -np.sin(ring[ib].Rotation)
                rb[1][1] = np.cos(ring[ib].Rotation)
        else:
            rb = [[1.0, 0.0], [0.0, 1.0]]

        if hasattr(ring[ib], 'Offset'):
            tb = ring[ib].Offset
        else:
            tb = [0.0, 0.0]

        if hasattr(ring[ib], 'Scale'):
            scale = ring[ib].Scale
        else:
            scale = [1.0, 1.0]

        if hasattr(ring[ib], 'Reading'):
            t_rand = ring[ib].Reading
        else:
            t_rand = [0.0, 0.0]

        t_rand = np.array(t_rand)
        t1 = np.array(t1)
        tb = np.array(tb)
        r1 = np.array(r1)
        rb = np.array(rb)
        scale = np.array(scale)

        trand.append(t_rand)

        r_mat_prod = np.matmul(r1, rb)

        """
        print('r_mat_prod')
        print(r_mat_prod)
        print('r_mat * t1')
        print(t1)
        print(np.matmul(r_mat_prod, t1))
        print('tb')
        print(tb)
        print('r_mat @ t1 + tb')
        print(((r_mat_prod @ t1) + tb))
        print('scale')
        print(scale)
        print('scale * ((r_mat_prod @ t1) + tb) ')
        print(scale * ((r_mat_prod @ t1) + tb) )
        """

        tt = (scale * ( (r_mat_prod @ t1) + tb)).transpose()
        # print('tt')
        # print(tt)

        tel.append(tt)

        # print('(np.tile(scale, 2, 1))')
        # print((np.tile(scale, (2,1))))
        # print('np.tile(scale, 2, 1) * r_mat_prod)')
        # print((np.tile(scale, (2,1)) * r_mat_prod))
        rel.append((np.tile(scale, (2,1)) * r_mat_prod))

    return rel, tel, trand


def bpm_process(hor_pos, ver_pos, rel, tel, trand):
    # modify bpm readings according to offset, rotation and scale factors in ring
    new_hor_pos = []
    new_ver_pos = []

    pos = np.column_stack((hor_pos, ver_pos))
    # print(len(pos))

    for p, trasl, scal, ran in zip(pos, tel, rel, trand):
        # print('pos: {} , scale: {} , trasl {}, random {}'.format(p, scal, trasl, ran))
        # print(scal @ p)
        # print(scal @ p + trasl)
        new_pos =  scal @ p + trasl + ran*np.random.normal((2,1))
        # print('new pos: {} '.format(new_pos))
        new_hor_pos.append(new_pos[0])
        new_ver_pos.append(new_pos[1])

    return new_hor_pos, new_ver_pos


def bpm_matrices_direct(ring, ind_bpms, hor, ver):
    # read BPM matrices from lattice
    rel = []
    tel = []
    trand = []
    t1 = []
    r1 = []
    tb = []
    rb = []
    t_rand = []
    new_h = []
    new_v = []

    for ib, h, v in zip(ind_bpms, hor, ver):

        if hasattr(ring[ib], 'T1'):
            t1 = ring[ib].T1
        else:
            t1 = [0.0, 0.0]

        if hasattr(ring[ib], 'R1'):
            r1[0][0] = ring[ib].R1[0][0]
            r1[0][1] = ring[ib].R1[0][2]
            r1[1][0] = ring[ib].R1[2][0]
            r1[1][1] = ring[ib].R1[2][2]

        else:
            r1 = [[1.0, 0.0], [0.0, 1.0]]

        if hasattr(ring[ib], 'Rotation'):
            rb[0][0] = np.cos(ring[ib].Rotation[0])
            rb[0][1] = np.sin(ring[ib].Rotation[0])
            rb[1][0] = -np.sin(ring[ib].Rotation[0])
            rb[1][1] = np.cos(ring[ib].Rotation[0])

        else:
            rb = [[1.0, 0.0], [0.0, 1.0]]

        if hasattr(ring[ib], 'Offset'):
            tb = ring[ib].Offset
        else:
            tb = [0.0, 0.0]

        if hasattr(ring[ib], 'Scale'):
            scale = ring[ib].Scale
        else:
            scale = [1.0, 1.0]

        if hasattr(ring[ib], 'Reading'):
            t_rand = ring[ib].Reading
        else:
            t_rand = [0.0, 0.0]

        t_rand = np.array(t_rand)
        t1 = np.array(t1)
        tb = np.array(tb)
        r1 = np.array(r1)
        rb = np.array(rb)
        scale = np.array(scale)

        trand.append(t_rand)

        r_mat_prod = np.matmul(r1, rb)

        tel.append(np.dot(scale, np.matmul(r_mat_prod, t1) + tb))

        rel.append(np.dot(np.array([scale, scale]), r_mat_prod))

        new_h.append(h)
        new_v.append(v)

    return new_h, new_v


def example_bpm_err():

    import at
    import time

    # path_to_lattice_model = "/machfs/liuzzo/EBS/beamdyn/matlab/optics/sr/theory/betamodel.mat"
    # ring = at.load_lattice(path_to_lattice_model, key='betamodel')

    path_to_lattice_model = "/machfs/liuzzo/EBS/minibeta_ID31_x7m_y1m/ANALYSIS/DA_TLT_IE/" \
                            "minibeta_ID31_x7m_y1m_53_30_betamodel/" \
                            "RefLatModelminibeta_ID31_x7m_y1m_53_30_betamodel_ns10/" \
                            "Errors_0003.mat"

    # path_to_lattice_model = "/machfs/liuzzo/EBS/S28F_all_BM_22Aug2021/ANALYSIS/" \
    #                        "DA_TLT_IE_ErrorsCorrection/S28F_all_BM_22Aug2021_53_30/" \
    #                        "RefLatModelS28F_all_BM_22Aug2021_53_30_ns10/" \
    #                        "Errors_0002.mat"

    ring = at.load_lattice(path_to_lattice_model, key='rcor')

    ring = at.Lattice(ring)

    # add some orbit
    ring[15].PolynomB[0] = 20e-6
    ring[15].PolynomA[0] = 20e-6
    print(ring[15])
    time.sleep(1)

    elems = ring.get_elements('BPM*')
    ind_bpms = [ring.index(elem) for elem in elems]

    # introduce known errors at given BPMs
    print(ind_bpms[45])
    print(ring[ind_bpms[45]])

    ring[ind_bpms[45]].Offset = [-234e-6, 334e-6]

    ring[ind_bpms[318]].Offset = [-24e-6, -334e-6]

    ring[ind_bpms[300]].Rotation = [3.14]

    ring[ind_bpms[200]].Scale = [1.1, 0.6]

    ring[ind_bpms[100]].Reading = [100e-6, 60e-6]

    # ring.radiation_off()
    # _orb_, orb = at.find_sync_orbit(ring, refpts=ind_bpms)

    ring.radiation_on()
    _orb_, orb = at.find_orbit6(ring, refpts=ind_bpms)

    rel, tel, trand = bpm_matrices(ring, ind_bpms)

    h = orb[:, 0]
    v = orb[:, 2]

    print(len(h))
    print(len(v))
    print(len(tel))
    print(len(rel))
    print(len(trand))

    he, ve = bpm_process(h, v, rel, tel, trand)

    # read again to see effect of Reading error

    # _orb_, orb = at.find_sync_orbit(ring, refpts=ind_bpms)

    ring.radiation_on()
    _orb_, orb = at.find_orbit6(ring, refpts=ind_bpms)

    h2 = orb[:, 0]
    v2 = orb[:, 2]
    he2, ve2 = bpm_process(h2, v2, rel, tel, trand)

    # plot effect of errors
    import matplotlib.pyplot as plt

    fig, (ax1, ax2) = plt.subplots(2, 1)
    ax1.plot(h, label='Horizontal initial')
    ax1.plot(he, label='Error')
    ax1.plot(he2, label='Error2')
    ax1.legend()

    ax2.plot(v, label='vertical initial')
    ax2.plot(ve, label='Error')
    ax2.plot(ve2, label='Error2')
    ax2.legend()

    plt.show()


if __name__ == '__main__':

    # to test:
    # >>> python
    # >>> from pyringsimulator import bpm_modifications as bpm_mod
    # >>> bpm_mod.example_bpm_err()

    # example_bpm_err()
    pass


